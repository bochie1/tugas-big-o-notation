// constant time O(1)
function test(str) {
  console.log(str);
}

test("constant time O(1)");

// quadratic time
// Linear Search
const linearSearch = (array, value) => {
  // Iterate Over Array
  for (let i = 0; i <= array.length; i++) {
    if (array[i] === value) {
      return i;
    }
  }

  // Return -1 If Not Found
  return -1;
};
